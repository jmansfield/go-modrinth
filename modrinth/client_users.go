/*
 * Copyright (c) 2022, Jamie Mansfield <jmansfield@cadixdev.org>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package modrinth

import (
	"encoding/json"
	"net/http"
)

// UsersService handles communication with the users routes of the Modrinth API.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/users
type UsersService service

// Get gets a user.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/users/operation/getUser
func (s *UsersService) Get(idOrUsername string) (*User, error) {
	request, err := s.client.NewRequest(http.MethodGet, "user/"+idOrUsername, nil)
	if err != nil {
		return nil, err
	}

	var response User
	_, err = s.client.Do(request, &response)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

type ModifyUserOptions struct {
	Username *string `json:"username,omitempty"`
	Name     *string `json:"name,omitempty"`
	Email    *string `json:"email,omitempty"`
	Bio      *string `json:"bio,omitempty"`
}

// Modify modifies a user.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/users/operation/modifyUser
func (s *UsersService) Modify(idOrUsername string, options ModifyUserOptions) error {
	request, err := s.client.NewRequest(http.MethodPatch, "user/"+idOrUsername, options)
	if err != nil {
		return nil
	}

	_, err = s.client.Do(request, nil)
	if err != nil {
		return err
	}

	return nil
}

// Delete deletes a user.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/users/operation/deleteUser
func (s *UsersService) Delete(idOrUsername string) error {
	request, err := s.client.NewRequest(http.MethodDelete, "user/"+idOrUsername, nil)
	if err != nil {
		return err
	}

	_, err = s.client.Do(request, nil)
	if err != nil {
		return err
	}

	return nil
}

// GetFromAuthHeader gets the currently authenticated user.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/users/operation/getUserFromAuth
func (s *UsersService) GetFromAuthHeader() (*User, error) {
	request, err := s.client.NewRequest(http.MethodGet, "user", nil)
	if err != nil {
		return nil, err
	}

	var response User
	_, err = s.client.Do(request, &response)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

// GetMultiple gets multiple users.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/users/operation/getUsers
func (s *UsersService) GetMultiple(idOrUsernames []string) ([]*User, error) {
	request, err := s.client.NewRequest(http.MethodGet, "users", nil)
	if err != nil {
		return nil, err
	}

	query := request.URL.Query()
	{
		out, err := json.Marshal(idOrUsernames)
		if err != nil {
			return nil, err
		}
		query.Add("ids", string(out))
	}
	request.URL.RawQuery = query.Encode()

	var response []*User
	_, err = s.client.Do(request, &response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

// GetProjects gets all a user's projects.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/users/operation/getUserProjects
func (s *UsersService) GetProjects(idOrUsername string) ([]*Project, error) {
	request, err := s.client.NewRequest(http.MethodGet, "user/"+idOrUsername+"/projects", nil)
	if err != nil {
		return nil, err
	}

	var response []*Project
	_, err = s.client.Do(request, &response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

// GetNotifications gets all a user's notifications.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/users/operation/getNotifications
func (s *UsersService) GetNotifications(idOrUsername string) ([]*Notification, error) {
	request, err := s.client.NewRequest(http.MethodGet, "user/"+idOrUsername+"/notifications", nil)
	if err != nil {
		return nil, err
	}

	var response []*Notification
	_, err = s.client.Do(request, &response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

// GetFollowedProjects gets all a user's followed projects.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/users/operation/getFollowedProjects
func (s *UsersService) GetFollowedProjects(idOrUsername string) ([]*Project, error) {
	request, err := s.client.NewRequest(http.MethodGet, "user/"+idOrUsername+"/follows", nil)
	if err != nil {
		return nil, err
	}

	var response []*Project
	_, err = s.client.Do(request, &response)
	if err != nil {
		return nil, err
	}

	return response, nil
}
