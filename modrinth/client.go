/*
 * Copyright (c) 2022, Jamie Mansfield <jmansfield@cadixdev.org>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package modrinth

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
)

const (
	defaultBaseURL   = "https://api.modrinth.com/v2/"
	defaultUserAgent = "go-modrinth"
)

// Client manages communication with the Modrinth API.
type Client struct {
	// HTTP client used to communicate with the API.
	client *http.Client

	// Base URL for API requests. BaseURL should always be set with a
	// trailing slash.
	BaseURL *url.URL

	// User Agent used when communicating with the Modrinth API.
	UserAgent string

	// Token used for authorisation with the Modrinth API.
	//
	// This is a GitHub token.
	Token string

	// Services used for accessing different parts of the Modrinth
	// API.
	Projects     *ProjectsService
	Versions     *VersionsService
	VersionFiles *VersionFilesService
	Users        *UsersService
	Teams        *TeamsService
	Tags         *TagsService
	Misc         *MiscellaneousService
}

type service struct {
	client *Client
}

// NewClient returns a new Modrinth API client. If a nil client is
// provided, http.DefaultClient will be used.
func NewClient(httpClient *http.Client) *Client {
	if httpClient == nil {
		httpClient = http.DefaultClient
	}
	baseURL, _ := url.Parse(defaultBaseURL)

	c := &Client{
		client:    httpClient,
		BaseURL:   baseURL,
		UserAgent: defaultUserAgent,
	}
	c.Projects = &ProjectsService{client: c}
	c.Versions = &VersionsService{client: c}
	c.VersionFiles = &VersionFilesService{client: c}
	c.Users = &UsersService{client: c}
	c.Teams = &TeamsService{client: c}
	c.Tags = &TagsService{client: c}
	c.Misc = &MiscellaneousService{client: c}
	return c
}

// NewRequest creates an API request. A relative URL can be provided
// in urlStr, in which case it is resolved to the BaseURL of the Client.
// Relative URLs should always be specified without a preceding slash.
// If specified, the value pointed to by the body is JSON encoded and
// included as the request body.
func (c *Client) NewRequest(method string, urlStr string, body interface{}) (*http.Request, error) {
	// Resolve absolute URL
	u, err := c.BaseURL.Parse(urlStr)
	if err != nil {
		return nil, err
	}

	// Encode body as JSON
	var buf io.ReadWriter
	if body != nil {
		buf = &bytes.Buffer{}
		enc := json.NewEncoder(buf)
		enc.SetEscapeHTML(false)
		err := enc.Encode(body)
		if err != nil {
			return nil, err
		}
	}

	// Create the request
	req, err := http.NewRequest(method, u.String(), buf)
	if err != nil {
		return nil, err
	}

	// Set request headers
	if body != nil {
		req.Header.Set("Content-Type", "application/json")
	}
	req.Header.Set("Accept", "application/json")
	if c.UserAgent != "" {
		req.Header.Set("User-Agent", c.UserAgent)
	}
	if c.Token != "" {
		req.Header.Set("Authorization", c.Token)
	}

	return req, nil
}

// Do sends an API request and returns the API response. The API response
// is JSON decoded and stored in the value pointed to by v.
func (c *Client) Do(req *http.Request, v interface{}) (*http.Response, error) {
	resp, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	err = CheckResponse(resp)
	if err != nil {
		return resp, err
	}

	if v != nil {
		err = json.NewDecoder(resp.Body).Decode(v)
	}

	return resp, err
}

func CheckResponse(r *http.Response) error {
	if r.StatusCode == http.StatusNotFound {
		return &NotFoundErrorResponse{Response: r}
	}
	if 200 <= r.StatusCode && r.StatusCode <= 299 {
		return nil
	}

	errorResponse := &ErrorResponse{
		Response: r,
	}

	data, err := io.ReadAll(r.Body)
	if err == nil && data != nil {
		err := json.Unmarshal(data, errorResponse)
		if err != nil {
			return err
		}
	}

	return errorResponse
}

type ErrorResponse struct {
	Response *http.Response

	ErrorType   string `json:"error"`
	Description string `json:"description"`
}

var _ error = (*ErrorResponse)(nil)

func (r *ErrorResponse) Error() string {
	return fmt.Sprintf("%v %v: %d error type: '%v' - %v",
		r.Response.Request.Method, r.Response.Request.URL,
		r.Response.StatusCode, r.ErrorType, r.Description)
}

// NotFoundErrorResponse occurs when Modrinth return a 404 Not Found response.
type NotFoundErrorResponse struct {
	Response *http.Response
}

var _ error = (*NotFoundErrorResponse)(nil)

func (r *NotFoundErrorResponse) Error() string {
	return fmt.Sprintf("%v %v: %d",
		r.Response.Request.Method, r.Response.Request.URL,
		r.Response.StatusCode)
}
