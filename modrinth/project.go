/*
 * Copyright (c) 2022, Jamie Mansfield <jmansfield@cadixdev.org>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package modrinth

import "time"

const (
	// SideRequired indicates that the project is required on the side.
	SideRequired = "required"

	// SideOptional indicates that the project is optional on the side.
	SideOptional = "optional"

	// SideUnsupported indicates that the project is unsupported on the side.
	SideUnsupported = "unsupported"
)

type DonationURL struct {
	ID       *string `json:"id,omitempty"`
	Platform *string `json:"platform,omitempty"`
	URL      *string `json:"url,omitempty"`
}

type Licence struct {
	ID   *string `json:"id,omitempty"`
	Name *string `json:"name,omitempty"`
	URL  *string `json:"url,omitempty"`
}

type GalleryImage struct {
	Title       *string    `json:"title,omitempty"`
	Description *string    `json:"description,omitempty"`
	Featured    *bool      `json:"featured,omitempty"`
	Created     *time.Time `json:"created,omitempty"`
	URL         *string    `json:"url,omitempty"`
}

type ModeratorMessage struct {
	Message *string `json:"message,omitempty"`
	Body    *string `json:"body,omitempty"`
}

type Project struct {
	Slug                 *string           `json:"slug,omitempty"`
	Title                *string           `json:"title,omitempty"`
	Description          *string           `json:"description,omitempty"`
	Categories           []string          `json:"categories,omitempty"`
	AdditionalCategories []string          `json:"additional_categories,omitempty"`
	ClientSide           *string           `json:"client_side,omitempty"`
	ServerSide           *string           `json:"server_side,omitempty"`
	Body                 *string           `json:"body,omitempty"`
	IssuesURL            *string           `json:"issues_url,omitempty"`
	SourceURL            *string           `json:"source_url,omitempty"`
	WikiURL              *string           `json:"wiki_url,omitempty"`
	DiscordURL           *string           `json:"discord_url,omitempty"`
	DonationURLs         []*DonationURL    `json:"donation_urls,omitempty"`
	ProjectType          *string           `json:"project_type,omitempty"`
	Downloads            *uint32           `json:"downloads,omitempty"`
	IconURL              *string           `json:"icon_url,omitempty"`
	Colour               *uint32           `json:"color,omitempty"`
	ID                   *string           `json:"id,omitempty"`
	Team                 *string           `json:"team,omitempty"`
	BodyURL              *string           `json:"body_url,omitempty"`
	ModeratorMessage     *ModeratorMessage `json:"moderator_message,omitempty"`
	Published            *time.Time        `json:"published,omitempty"`
	Updated              *time.Time        `json:"updated,omitempty"`
	Approved             *time.Time        `json:"approved,omitempty"`
	Followers            *uint32           `json:"followers,omitempty"`
	Status               *string           `json:"status,omitempty"`
	Licence              *Licence          `json:"license,omitempty"`
	Versions             []string          `json:"versions,omitempty"`
	GameVersions         []string          `json:"game_versions,omitempty"`
	Loaders              []string          `json:"loaders,omitempty"`
	Gallery              []*GalleryImage   `json:"gallery,omitempty"`
}

type ProjectDependencies struct {
	Projects []*Project `json:"projects,omitempty"`
	Versions []*Version `json:"versions,omitempty"`
}
