/*
 * Copyright (c) 2022, Jamie Mansfield <jmansfield@cadixdev.org>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package modrinth

import (
	"encoding/json"
	"net/http"
	"strconv"
)

// VersionsService handles communication with the versions routes of the Modrinth API.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/versions
type VersionsService service

type ListVersionsOptions struct {
	Loaders      []string
	GameVersions []string
	Featured     *bool
}

// ListVersions gets all versions of a project.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/versions/operation/getProjectVersions
func (s *VersionsService) ListVersions(projectIDOrSlug string, options *ListVersionsOptions) ([]*Version, error) {
	request, err := s.client.NewRequest(http.MethodGet, "project/"+projectIDOrSlug+"/version", nil)
	if err != nil {
		return nil, err
	}

	if options != nil {
		query := request.URL.Query()

		if len(options.Loaders) > 0 {
			out, err := json.Marshal(options.Loaders)
			if err != nil {
				return nil, err
			}
			query.Add("loaders", string(out))
		}

		if len(options.GameVersions) > 0 {
			out, err := json.Marshal(options.GameVersions)
			if err != nil {
				return nil, err
			}
			query.Add("game_versions", string(out))
		}

		if options.Featured != nil {
			query.Add("featured", strconv.FormatBool(*options.Featured))
		}

		request.URL.RawQuery = query.Encode()
	}

	var response []*Version
	_, err = s.client.Do(request, &response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

// Get gets a version.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/versions/operation/getVersion
func (s *VersionsService) Get(idOrSlug string) (*Version, error) {
	request, err := s.client.NewRequest(http.MethodGet, "version/"+idOrSlug, nil)
	if err != nil {
		return nil, err
	}

	var response Version
	_, err = s.client.Do(request, &response)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

type ModifyVersionOptions struct {
	Name          *string       `json:"name,omitempty"`
	VersionNumber *string       `json:"version_number,omitempty"`
	Changelog     *string       `json:"changelog,omitempty"`
	VersionType   *string       `json:"version_type,omitempty"`
	Dependencies  []*Dependency `json:"dependencies,omitempty"`
	GameVersions  []string      `json:"game_versions,omitempty"`
	Loaders       []string      `json:"loaders,omitempty"`
	Featured      *bool         `json:"featured,omitempty"`
	PrimaryFile   []string      `json:"primary_file,omitempty"`
}

// Modify modifies a version.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/versions/operation/modifyVersion
func (s *VersionsService) Modify(id string, options ModifyVersionOptions) error {
	request, err := s.client.NewRequest(http.MethodPatch, "version/"+id, options)
	if err != nil {
		return err
	}

	_, err = s.client.Do(request, nil)
	if err != nil {
		return err
	}

	return nil
}

// Delete deletes a version.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/versions/operation/deleteVersion
func (s *VersionsService) Delete(idOrSlug string) error {
	request, err := s.client.NewRequest(http.MethodDelete, "version/"+idOrSlug, nil)
	if err != nil {
		return err
	}

	_, err = s.client.Do(request, nil)
	if err != nil {
		return err
	}

	return nil
}

// GetMultiple gets multiple versions.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/versions/operation/getVersions
func (s *VersionsService) GetMultiple(ids []string) ([]*Version, error) {
	request, err := s.client.NewRequest(http.MethodGet, "versions", nil)
	if err != nil {
		return nil, err
	}

	query := request.URL.Query()
	{
		out, err := json.Marshal(ids)
		if err != nil {
			return nil, err
		}
		query.Add("ids", string(out))
	}
	request.URL.RawQuery = query.Encode()

	var response []*Version
	_, err = s.client.Do(request, &response)
	if err != nil {
		return nil, err
	}

	return response, nil
}
