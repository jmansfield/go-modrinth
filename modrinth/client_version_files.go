/*
 * Copyright (c) 2022, Jamie Mansfield <jmansfield@cadixdev.org>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package modrinth

import "net/http"

// VersionFilesService handles communication with the version files routes of the Modrinth API.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/version-files
type VersionFilesService service

// GetFromHash gets a version from a file's hash.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/version-files/operation/versionFromHash
func (s *VersionFilesService) GetFromHash(hash string, algorithm string) (*Version, error) {
	request, err := s.client.NewRequest(http.MethodGet, "version_file/"+hash, nil)
	if err != nil {
		return nil, err
	}
	if algorithm != "" {
		query := request.URL.Query()
		query.Add("algorithm", algorithm)
		request.URL.RawQuery = query.Encode()
	}

	var response Version
	_, err = s.client.Do(request, &response)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

// DeleteFromHash deletes a file from a version based on its hash.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/version-files/operation/deleteFileFromHash
func (s *VersionFilesService) DeleteFromHash(hash string, algorithm string) error {
	request, err := s.client.NewRequest(http.MethodDelete, "version_file/"+hash, nil)
	if err != nil {
		return err
	}
	if algorithm != "" {
		query := request.URL.Query()
		query.Add("algorithm", algorithm)
		request.URL.RawQuery = query.Encode()
	}

	_, err = s.client.Do(request, nil)
	if err != nil {
		return err
	}

	return nil
}

// GetLatestVersionFromHash gets the latest version of a project, based on a hash of a project's version's file.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/version-files/operation/getLatestVersionFromHash
func (s *VersionFilesService) GetLatestVersionFromHash(
	hash string, algorithm string, loaders []string, gameVersions []string,
) (*Version, error) {
	var data = struct {
		Loaders      []string `json:"loaders"`
		GameVersions []string `json:"game_versions"`
	}{
		Loaders:      loaders,
		GameVersions: gameVersions,
	}

	request, err := s.client.NewRequest(http.MethodGet, "version_file/"+hash+"/update", &data)
	if err != nil {
		return nil, err
	}
	if algorithm != "" {
		query := request.URL.Query()
		query.Add("algorithm", algorithm)
		request.URL.RawQuery = query.Encode()
	}

	var response Version
	_, err = s.client.Do(request, &response)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

// GetFromHashes gets multiple versions from their file's hashes.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/version-files/operation/versionsFromHashes
func (s *VersionFilesService) GetFromHashes(hashes []string, algorithm string) (map[string]*Version, error) {
	var data = struct {
		Hashes    []string `json:"hashes"`
		Algorithm string   `json:"algorithm"`
	}{
		Hashes:    hashes,
		Algorithm: algorithm,
	}

	request, err := s.client.NewRequest(http.MethodPost, "version_files", &data)
	if err != nil {
		return nil, err
	}

	var response map[string]*Version
	_, err = s.client.Do(request, &response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

// GetLatestVersionsFromHashes gets the latest versions of projects, based on hashes from version's files.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/version-files/operation/getLatestVersionsFromHashes
func (s *VersionFilesService) GetLatestVersionsFromHashes(
	hashes []string, algorithm string, loaders []string, gameVersions []string,
) (map[string]*Version, error) {
	var data = struct {
		Hashes       []string `json:"hashes"`
		Algorithm    string   `json:"algorithm"`
		Loaders      []string `json:"loaders"`
		GameVersions []string `json:"game_versions"`
	}{
		Hashes:       hashes,
		Algorithm:    algorithm,
		Loaders:      loaders,
		GameVersions: gameVersions,
	}

	request, err := s.client.NewRequest(http.MethodPost, "version_files", &data)
	if err != nil {
		return nil, err
	}

	var response map[string]*Version
	_, err = s.client.Do(request, &response)
	if err != nil {
		return nil, err
	}

	return response, nil
}
