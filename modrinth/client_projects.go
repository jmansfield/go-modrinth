/*
 * Copyright (c) 2022, Jamie Mansfield <jmansfield@cadixdev.org>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package modrinth

import (
	"encoding/json"
	"net/http"
	"strconv"
)

// ProjectsService handles communication with the projects routes of the Modrinth API.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/projects
type ProjectsService service

type SearchOptions struct {
	Query   string
	Facets  [][]string
	Index   string
	Offset  int
	Limit   int
	Filters string
	Version string
}

const (
	// SearchFacetCategories filters by project's categories.
	SearchFacetCategories = "categories"

	// SearchFacetVersions filters by Minecraft version.
	SearchFacetVersions = "versions"

	// SearchFacetLicence filters by project's licence.
	SearchFacetLicence = "license"

	// SearchFacetProjectType filters by project's type.
	SearchFacetProjectType = "project_type"
)

const (
	// SearchIndexRelevance sorts search results by Modrinth's assessment of relevance.
	SearchIndexRelevance = "relevance"

	// SearchIndexDownloads sorts search results by the order of downloads.
	SearchIndexDownloads = "downloads"

	// SearchIndexFollows sorts search results by the order of project followers.
	SearchIndexFollows = "follows"

	// SearchIndexNewest sorts search results by the newest projects.
	SearchIndexNewest = "newest"

	// SearchIndexUpdated sorts search results by the latest updated projects.
	SearchIndexUpdated = "updated"
)

// Search searches all projects based on the criteria provided, and returns search results
// in a paginated response.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/projects/operation/searchProjects
func (s *ProjectsService) Search(options *SearchOptions) (*SearchResponse, error) {
	request, err := s.client.NewRequest(http.MethodGet, "search", nil)
	if err != nil {
		return nil, err
	}

	query := request.URL.Query()

	if options.Query != "" {
		query.Add("query", options.Query)
	}
	if options.Facets != nil && len(options.Facets) != 0 {
		out, err := json.Marshal(options.Facets)
		if err != nil {
			return nil, err
		}
		query.Add("facets", string(out))
	}
	if options.Index != "" {
		query.Add("index", options.Index)
	}
	if options.Offset != 0 {
		query.Add("offset", strconv.Itoa(options.Offset))
	}
	if options.Limit != 0 {
		query.Add("limit", strconv.Itoa(options.Limit))
	}
	if options.Filters != "" {
		query.Add("filters", options.Filters)
	}
	if options.Version != "" {
		query.Add("version", options.Version)
	}

	request.URL.RawQuery = query.Encode()

	var response SearchResponse
	_, err = s.client.Do(request, &response)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

// Get gets a project.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/projects/operation/getProject
func (s *ProjectsService) Get(idOrSlug string) (*Project, error) {
	request, err := s.client.NewRequest(http.MethodGet, "project/"+idOrSlug, nil)
	if err != nil {
		return nil, err
	}

	var response Project
	_, err = s.client.Do(request, &response)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

type ModifyProjectOptions struct {
	Title                 *string        `json:"title,omitempty"`
	Description           *string        `json:"description,omitempty"`
	Body                  *string        `json:"body,omitempty"`
	Categories            []string       `json:"categories,omitempty"`
	IssuesURL             *string        `json:"issues_url,omitempty"`
	SourceURL             *string        `json:"source_url,omitempty"`
	WikiURL               *string        `json:"wiki_url,omitempty"`
	LicenceURL            *string        `json:"license_url,omitempty"`
	DiscordURL            *string        `json:"discord_url,omitempty"`
	DonationURLs          []*DonationURL `json:"donation_urls,omitempty"`
	LicenceID             *string        `json:"license_id,omitempty"`
	ClientSide            *string        `json:"client_side,omitempty"`
	ServerSide            *string        `json:"server_side,omitempty"`
	Slug                  *string        `json:"slug,omitempty"`
	Status                *string        `json:"status,omitempty"`
	ModerationMessage     *string        `json:"moderation_message,omitempty"`
	ModerationMessageBody *string        `json:"moderation_message_body,omitempty"`
}

// Modify modifies a project.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/projects/operation/modifyProject
func (s *ProjectsService) Modify(idOrSlug string, options ModifyProjectOptions) error {
	request, err := s.client.NewRequest(http.MethodPatch, "project/"+idOrSlug, options)
	if err != nil {
		return err
	}

	_, err = s.client.Do(request, nil)
	if err != nil {
		return err
	}

	return nil
}

// Delete deletes a project.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/projects/operation/deleteProject
func (s *ProjectsService) Delete(idOrSlug string) error {
	request, err := s.client.NewRequest(http.MethodDelete, "project/"+idOrSlug, nil)
	if err != nil {
		return err
	}

	_, err = s.client.Do(request, nil)
	if err != nil {
		return err
	}

	return nil
}

// GetMultiple gets multiples projects.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/projects/operation/getProjects
func (s *ProjectsService) GetMultiple(ids []string) ([]*Project, error) {
	request, err := s.client.NewRequest(http.MethodGet, "projects", nil)
	if err != nil {
		return nil, err
	}

	query := request.URL.Query()
	{
		out, err := json.Marshal(ids)
		if err != nil {
			return nil, err
		}
		query.Add("ids", string(out))
	}
	request.URL.RawQuery = query.Encode()

	var response []*Project
	_, err = s.client.Do(request, &response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

type GalleryImageOptions struct {
	Featured    bool
	Name        string
	Description string
}

// ModifyGalleryImage modifies the name and description of a gallery image from a project,
// based on it's URL.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/projects/operation/modifyGalleryImage
func (s *ProjectsService) ModifyGalleryImage(idOrSlug string, url string, options *GalleryImageOptions) error {
	request, err := s.client.NewRequest(http.MethodPatch, "project/"+idOrSlug+"/gallery", nil)
	if err != nil {
		return err
	}

	query := request.URL.Query()
	query.Add("url", url)
	query.Add("featured", strconv.FormatBool(options.Featured))
	if options.Name != "" {
		query.Add("name", options.Name)
	}
	if options.Description != "" {
		query.Add("description", options.Description)
	}
	request.URL.RawQuery = query.Encode()

	_, err = s.client.Do(request, nil)
	if err != nil {
		return err
	}

	return nil
}

// DeleteGalleryImage deletes a gallery image from a project, based on its URL.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/projects/operation/deleteGalleryImage
func (s *ProjectsService) DeleteGalleryImage(idOrSlug string, url string) error {
	request, err := s.client.NewRequest(http.MethodDelete, "project/"+idOrSlug+"/gallery", nil)
	if err != nil {
		return err
	}
	query := request.URL.Query()
	query.Add("url", url)
	request.URL.RawQuery = query.Encode()

	_, err = s.client.Do(request, nil)
	if err != nil {
		return err
	}

	return nil
}

// GetDependencies gets the project and version dependencies of a project.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/projects/operation/getDependencies
func (s *ProjectsService) GetDependencies(idOrSlug string) (*ProjectDependencies, error) {
	request, err := s.client.NewRequest(http.MethodGet, "project/"+idOrSlug+"/dependencies", nil)
	if err != nil {
		return nil, err
	}

	var response ProjectDependencies
	_, err = s.client.Do(request, &response)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

// Follow causes the authenticated user to follow the project.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/projects/operation/followProject
func (s *ProjectsService) Follow(idOrSlug string) error {
	request, err := s.client.NewRequest(http.MethodPost, "project/"+idOrSlug+"/follow", nil)
	if err != nil {
		return err
	}

	_, err = s.client.Do(request, nil)
	if err != nil {
		return err
	}

	return nil
}

// Unfollow causes the authenticated user to unfollow the project.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/projects/operation/unfollowProject
func (s *ProjectsService) Unfollow(idOrSlug string) error {
	request, err := s.client.NewRequest(http.MethodDelete, "project/"+idOrSlug+"/follow", nil)
	if err != nil {
		return err
	}

	_, err = s.client.Do(request, nil)
	if err != nil {
		return err
	}

	return nil
}
