/*
 * Copyright (c) 2022, Jamie Mansfield <jmansfield@cadixdev.org>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package modrinth

import "net/http"

// TagsService handles communication with the tags routes of the Modrinth API.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/tags
type TagsService service

// GetCategories gets all category tags available for usage.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/tags/operation/categoryList
func (s *TagsService) GetCategories() ([]*CategoryTag, error) {
	request, err := s.client.NewRequest(http.MethodGet, "tag/category", nil)
	if err != nil {
		return nil, err
	}

	var response []*CategoryTag
	_, err = s.client.Do(request, &response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

// GetLoaders gets all loader tags available for usage.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/tags/operation/loaderList
func (s *TagsService) GetLoaders() ([]*LoaderTag, error) {
	request, err := s.client.NewRequest(http.MethodGet, "tag/loader", nil)
	if err != nil {
		return nil, err
	}

	var response []*LoaderTag
	_, err = s.client.Do(request, &response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

// GetGameVersions gets all game version tags available for usage.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/tags/operation/versionList
func (s *TagsService) GetGameVersions() ([]*GameVersionTag, error) {
	request, err := s.client.NewRequest(http.MethodGet, "tag/game_version", nil)
	if err != nil {
		return nil, err
	}

	var response []*GameVersionTag
	_, err = s.client.Do(request, &response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

// GetLicences gets all licence tags available for usage.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/tags/operation/licenseList
func (s *TagsService) GetLicences() ([]*LicenceTag, error) {
	request, err := s.client.NewRequest(http.MethodGet, "tag/license", nil)
	if err != nil {
		return nil, err
	}

	var response []*LicenceTag
	_, err = s.client.Do(request, &response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

// GetDonationPlatforms gets all donation platform tags available for usage.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/tags/operation/donationPlatformList
func (s *TagsService) GetDonationPlatforms() ([]*DonationPlatformTag, error) {
	request, err := s.client.NewRequest(http.MethodGet, "tag/donation_platform", nil)
	if err != nil {
		return nil, err
	}

	var response []*DonationPlatformTag
	_, err = s.client.Do(request, &response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

// GetReportTypes gets all report type tags available for usage.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/tags/operation/reportTypeList
func (s *TagsService) GetReportTypes() ([]string, error) {
	request, err := s.client.NewRequest(http.MethodGet, "tag/report_type", nil)
	if err != nil {
		return nil, err
	}

	var response []string
	_, err = s.client.Do(request, &response)
	if err != nil {
		return nil, err
	}

	return response, nil
}
