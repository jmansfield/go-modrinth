/*
 * Copyright (c) 2022, Jamie Mansfield <jmansfield@cadixdev.org>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package modrinth

import "time"

type Dependency struct {
	ProjectID      *string `json:"project_id,omitempty"`
	VersionID      *string `json:"version_id,omitempty"`
	DependencyType *string `json:"dependency_type,omitempty"`
}

type File struct {
	Hashes   map[string]string `json:"hashes,omitempty"`
	URL      *string           `json:"url,omitempty"`
	Filename *string           `json:"filename,omitempty"`
	Primary  *bool             `json:"primary,omitempty"`
	Size     *uint32           `json:"size,omitempty"`
}

type Version struct {
	Name          *string       `json:"name,omitempty"`
	VersionNumber *string       `json:"version_number,omitempty"`
	Changelog     *string       `json:"changelog,omitempty"`
	Dependencies  []*Dependency `json:"dependencies,omitempty"`
	GameVersions  []string      `json:"game_versions,omitempty"`
	VersionType   *string       `json:"version_type,omitempty"`
	Loaders       []string      `json:"loaders,omitempty"`
	Featured      *bool         `json:"featured,omitempty"`
	ID            *string       `json:"id,omitempty"`
	ProjectID     *string       `json:"project_id,omitempty"`
	AuthorID      *string       `json:"author_id,omitempty"`
	DatePublished *time.Time    `json:"date_published,omitempty"`
	Downloads     *uint32       `json:"downloads,omitempty"`
	ChangelogURL  *string       `json:"changelog_url,omitempty"`
	Files         []*File       `json:"files,omitempty"`
}
