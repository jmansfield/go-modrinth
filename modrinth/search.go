/*
 * Copyright (c) 2022, Jamie Mansfield <jmansfield@cadixdev.org>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package modrinth

import "time"

type SearchResponse struct {
	Hits      []*SearchResult `json:"hits,omitempty"`
	Offset    *uint32         `json:"offset,omitempty"`
	Limit     *uint32         `json:"limit,omitempty"`
	TotalHits *uint32         `json:"total_hits,omitempty"`
}

type SearchResult struct {
	Slug          *string    `json:"slug,omitempty"`
	Title         *string    `json:"title,omitempty"`
	Description   *string    `json:"description,omitempty"`
	Categories    []string   `json:"categories,omitempty"`
	ClientSide    *string    `json:"client_side,omitempty"`
	ServerSide    *string    `json:"server_side,omitempty"`
	ProjectType   *string    `json:"project_type,omitempty"`
	Downloads     *uint32    `json:"downloads,omitempty"`
	IconURL       *string    `json:"icon_url,omitempty"`
	ProjectID     *string    `json:"project_id,omitempty"`
	Author        *string    `json:"author,omitempty"`
	Versions      []string   `json:"versions,omitempty"`
	Follows       *uint32    `json:"follows,omitempty"`
	DateCreated   *time.Time `json:"date_created,omitempty"`
	DateModified  *time.Time `json:"date_modified,omitempty"`
	LatestVersion *string    `json:"latest_version,omitempty"`
	Licence       *string    `json:"license,omitempty"`
	Gallery       []string   `json:"gallery,omitempty"`
}
