/*
 * Copyright (c) 2022, Jamie Mansfield <jmansfield@cadixdev.org>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package modrinth

import (
	"encoding/json"
	"net/http"
)

// TeamsService handles communication with the teams routes of the Modrinth API.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/teams
type TeamsService service

// GetProjectTeam gets the team members from a project.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/teams/operation/getProjectTeamMembers
func (s *TeamsService) GetProjectTeam(idOrSlug string) ([]*TeamMember, error) {
	request, err := s.client.NewRequest(http.MethodGet, "project/"+idOrSlug+"/members", nil)
	if err != nil {
		return nil, err
	}

	var response []*TeamMember
	_, err = s.client.Do(request, &response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

// Get gets the team members from a team.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/teams/operation/getTeamMembers
func (s *TeamsService) Get(id string) ([]*TeamMember, error) {
	request, err := s.client.NewRequest(http.MethodGet, "team/"+id+"/members", nil)
	if err != nil {
		return nil, err
	}

	var response []*TeamMember
	_, err = s.client.Do(request, &response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

// AddMember invites a user to a team.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/teams/operation/addTeamMember
func (s *TeamsService) AddMember(teamID string, userID string) error {
	var data = struct {
		UserID string `json:"user_id"`
	}{
		UserID: userID,
	}

	request, err := s.client.NewRequest(http.MethodPost, "team/"+teamID+"/members", &data)
	if err != nil {
		return err
	}

	_, err = s.client.Do(request, nil)
	if err != nil {
		return err
	}

	return nil
}

// Join joins a team.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/teams/operation/joinTeam
func (s *TeamsService) Join(id string) error {
	request, err := s.client.NewRequest(http.MethodPost, "team/"+id+"/join", nil)
	if err != nil {
		return err
	}

	_, err = s.client.Do(request, nil)
	if err != nil {
		return err
	}

	return nil
}

// RemoveMember removes a member from a team.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/teams/paths/~1team~1{team_id}~1members~1{user_id}/delete
func (s *TeamsService) RemoveMember(teamID string, userIDOrUsername string) error {
	request, err := s.client.NewRequest(http.MethodDelete, "team/"+teamID+"/members/"+userIDOrUsername, nil)
	if err != nil {
		return err
	}

	_, err = s.client.Do(request, nil)
	if err != nil {
		return err
	}

	return nil
}

// TransferOwnership transfers ownership of a team to another user.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/teams/operation/transferTeamOwnership
func (s *TeamsService) TransferOwnership(teamID string, userID string) error {
	var data = struct {
		UserID string `json:"user_id"`
	}{
		UserID: userID,
	}

	request, err := s.client.NewRequest(http.MethodPost, "team/"+teamID+"/owner", &data)
	if err != nil {
		return err
	}

	_, err = s.client.Do(request, nil)
	if err != nil {
		return err
	}

	return nil
}

// GetMultiple gets multiples teams.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/teams/operation/getTeams
func (s *TeamsService) GetMultiple(ids []string) ([][]*TeamMember, error) {
	request, err := s.client.NewRequest(http.MethodGet, "teams", nil)
	if err != nil {
		return nil, err
	}

	query := request.URL.Query()
	{
		out, err := json.Marshal(ids)
		if err != nil {
			return nil, err
		}
		query.Add("ids", string(out))
	}
	request.URL.RawQuery = query.Encode()

	var response [][]*TeamMember
	_, err = s.client.Do(request, &response)
	if err != nil {
		return nil, err
	}

	return response, nil
}
