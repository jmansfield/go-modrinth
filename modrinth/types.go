/*
 * Copyright (c) 2022, Jamie Mansfield <jmansfield@cadixdev.org>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package modrinth

// Bool returns a pointer to the bool v.
func Bool(v bool) *bool {
	return &v
}

// UInt32 returns a pointer to the 32-bit unsigned int v.
func UInt32(v uint32) *uint32 {
	return &v
}

// UInt64 returns a pointer to the 64-bit unsigned int v.
func UInt64(v uint64) *uint64 {
	return &v
}

// String returns a pointer to the string v.
func String(v string) *string {
	return &v
}
