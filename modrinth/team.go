/*
 * Copyright (c) 2022, Jamie Mansfield <jmansfield@cadixdev.org>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package modrinth

type TeamMember struct {
	TeamID      *string `json:"team_id,omitempty"`
	User        *User   `json:"user,omitempty"`
	Role        *string `json:"role,omitempty"`
	Permissions *uint64 `json:"permissions,omitempty"`
	Accepted    *bool   `json:"accepted,omitempty"`
}
