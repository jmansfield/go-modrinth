/*
 * Copyright (c) 2022, Jamie Mansfield <jmansfield@cadixdev.org>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package modrinth

import "time"

type User struct {
	Username  *string    `json:"username,omitempty"`
	Name      *string    `json:"name,omitempty"`
	Email     *string    `json:"email,omitempty"`
	Bio       *string    `json:"bio,omitempty"`
	ID        *string    `json:"id,omitempty"`
	GitHubID  *uint64    `json:"github_id,omitempty"`
	AvatarURL *string    `json:"avatar_url,omitempty"`
	Created   *time.Time `json:"created,omitempty"`
	Role      *string    `json:"role,omitempty"`
}

type Notification struct {
	ID      *string    `json:"id,omitempty"`
	UserID  *string    `json:"user_id,omitempty"`
	Type    *string    `json:"type,omitempty"`
	Title   *string    `json:"title,omitempty"`
	Text    *string    `json:"text,omitempty"`
	Link    *string    `json:"link,omitempty"`
	Read    *bool      `json:"read,omitempty"`
	Created *time.Time `json:"created,omitempty"`
}
