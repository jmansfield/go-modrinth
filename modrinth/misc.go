/*
 * Copyright (c) 2022-2023, Jamie Mansfield <jmansfield@cadixdev.org>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package modrinth

import "time"

type Report struct {
	ReportType *string    `json:"report_type,omitempty"`
	ItemID     *string    `json:"item_id,omitempty"`
	ItemType   *string    `json:"item_type,omitempty"`
	Body       *string    `json:"body,omitempty"`
	Reporter   *string    `json:"reporter,omitempty"`
	Created    *time.Time `json:"created,omitempty"`
}

type Statistics struct {
	Projects uint64 `json:"projects"`
	Versions uint64 `json:"versions"`
	Files    uint64 `json:"files"`
	Authors  uint64 `json:"authors"`
}
