/*
 * Copyright (c) 2022, Jamie Mansfield <jmansfield@cadixdev.org>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package modrinth

import "time"

type CategoryTag struct {
	Icon        *string `json:"icon,omitempty"`
	Name        *string `json:"name,omitempty"`
	ProjectType *string `json:"project_type,omitempty"`
	Header      *string `json:"header,omitempty"`
}

type LoaderTag struct {
	Icon                  *string  `json:"icon,omitempty"`
	Name                  *string  `json:"name,omitempty"`
	SupportedProjectTypes []string `json:"supported_project_types,omitempty"`
}

type GameVersionTag struct {
	Version     *string    `json:"version,omitempty"`
	VersionType *string    `json:"version_type,omitempty"`
	Date        *time.Time `json:"date,omitempty"`
	Major       *bool      `json:"major,omitempty"`
}

type LicenceTag struct {
	Short *string `json:"short,omitempty"`
	Name  *string `json:"name,omitempty"`
}

type DonationPlatformTag struct {
	Short *string `json:"short,omitempty"`
	Name  *string `json:"name,omitempty"`
}
