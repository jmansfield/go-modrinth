/*
 * Copyright (c) 2022-2023, Jamie Mansfield <jmansfield@cadixdev.org>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package modrinth

import "net/http"

// MiscellaneousService handles communication with the miscellaneous
// routes of the Modrinth API.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/misc
type MiscellaneousService service

// Report reports a user.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/misc/operation/submitReport
func (s *MiscellaneousService) Report(
	reportType string, itemID string, itemType string, body string,
) (*Report, error) {
	var data = struct {
		ReportType string `json:"report_type"`
		ItemID     string `json:"item_id"`
		ItemType   string `json:"item_type"`
		Body       string `json:"body"`
	}{
		ReportType: reportType,
		ItemID:     itemID,
		ItemType:   itemType,
		Body:       body,
	}

	request, err := s.client.NewRequest(http.MethodPost, "report", &data)
	if err != nil {
		return nil, err
	}

	var response Report
	_, err = s.client.Do(request, &response)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

// GetStatistics gets statistics for the Modrinth API.
//
// Modrinth API docs: https://docs.modrinth.com/api-spec/#tag/misc/operation/statistics
func (s *MiscellaneousService) GetStatistics() (*Statistics, error) {
	request, err := s.client.NewRequest(http.MethodGet, "statistics", nil)
	if err != nil {
		return nil, err
	}

	var response *Statistics
	_, err = s.client.Do(request, &response)
	if err != nil {
		return nil, err
	}

	return response, nil
}
