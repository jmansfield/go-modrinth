go-modrinth
===

go-modrinth is a Go client library for accessing [Modrinth's API].

[Modrinth's API]: https://docs.modrinth.com/api-spec/
